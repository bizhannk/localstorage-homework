import { validate } from './email-validator.js';

const divEl = document.querySelector('#new-section');
const sectionEl = document.createElement('section');

sectionEl.className = 'join-us';

export const SectionCreator = (type = 'standart') => {
  let title = 'Join Our Program';
  let button = 'Subscribe';
  if (type === 'advanced') {
    title = 'Join Our Advanced Program';
    button = 'Subscribe to Advanced Program ';
  }
  return (sectionEl.innerHTML = `
    <h2>${title}</h2>
    <p class="subheading">Sed do eiusmod tempor incididunt
    ut labore et dolore magna aliqua.</p>
    <form>
      <input type="email" id="email" placeholder="Email"/>
      <button class="btn" id="btn" type="submit">${button} </button>
    </form>
  `);
};

export const render = () => {
  divEl.append(sectionEl);
  // get elements rendered from DOM
  const form = document.querySelector('form');
  const email = document.querySelector('#email');
  const btn = document.querySelector('#btn');

  // get localstorage
  const emailLs = localStorage.getItem('email');

  // set input field and button text
  emailLs ? (email.value = emailLs) : (email.value = '');
  emailLs ? (btn.textContent = 'Unsubscribe') : (btn.textContent = 'Subcribe');

  // set localstorage on input event
  email.addEventListener('input', (e) => {
    localStorage.setItem('email', email.value);
  });

  form.addEventListener('submit', (e) => {
    e.preventDefault();
    const isValid = validate(email.value);
    // change button text
    if (isValid) {
      btn.textContent = 'Unsubscribe';
    }
    // remove item from localstorage, switch button's text and clear input field
    btn.addEventListener('click', () => {
      if (btn.textContent === 'Unsubscribe') {
        localStorage.removeItem('email');
        btn.textContent = 'Subcribe';
        email.value = '';
      }
    });
  });
};
